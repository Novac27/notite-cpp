#include <iostream>
#include <stdlib.h>
#include <cstring>
#include <conio.h>
#include <string>
#include <algorithm>
#include <ctype.h>
using namespace std;


int menu()
{
    // Prezentam meniul.

    cout << "Buna ziua! Alegeti una dintre urmatoarele optiuni: " << endl << endl;
    cout << "1) Schimbarea literelor mici in litere mari." << endl;
    cout << "2) Frecventa unui cuvant." << endl;
    cout << "3) Numarul consoanelor/vocalelor." << endl;
    cout << "4) Crearea unui acronim." << endl;
    cout << "5) Verificarea daca doua cuvinte sunt anagrame." << endl;

    // Alegem o optiune.
    int alegere;
    cin >> alegere;
    system("CLS");  // Dam clear la consola.

    return alegere;  // Returnam alegerea facuta.

}

bool litMareInLitMica()
{
    //Prezentam optiunea aleasa.
    cout << "Ati ales schimbarea literelor mici in litere mari." << endl;

    //Creem un vector in care o sa stocam textul.
Scriere:
    char txt[501];              //Initializam vectorul.
    cout << "Introduceti textul: " << endl;

    cin.ignore();               //Ignoram acel new line pentru ca userul sa introduca textul dorit.
    cin.get(txt,501);           //Citim textul de la tastatura.
    cout << endl << endl;       //Padding
    system("CLS");              //Dam clear la consola.

    //Vrem o confirmare de la user ca textul este introdus corect.
Confirmare:
    cout << "Textul este acesta: " << endl;
    for(int i = 0 ; i < strlen(txt); i++) cout << txt[i];
    cout << endl << endl;

    //Verificam daca textul este introdus corect.
    cout << "Este textul scris corect? " << endl;
    cout << "1) Da." << endl << "2) Nu." << endl;

    //Alegem o optiune
    int confirmare;
    cin >> confirmare;
    system("CLS");

    //Daca textul este scris corect, transformam literele mici din text in litere mari.
    if(confirmare == 1)
    {
        for(int i = 0; i < strlen(txt); i++) txt[i]=toupper(txt[i]);    //Transformam literele mici in litere mari.
        system("CLS");                                                  //Dam clear la consola.
        cout << "Textul transformat: " << endl << endl;                 //Afisam textul transformat.
        for(int i = 0; i < strlen(txt); i++) cout << txt[i];
    }
    else if(confirmare == 2)        //Daca textul nu a fost introdus corect, punem userul sa introduca din nou textul
    {
        goto Scriere;
    }
    else                            //Daca userul a introdus alta optiune, il punem sa aleaga o optiune buna.
    {
        goto Confirmare;
    }

    //Intrebam userul daca mai doreste sa incerce ceva sau sa iasa din program.
    cout << endl << endl << endl;
    cout << "Doriti sa incercati altceva sau sa iesiti din program? "  << endl;
    cout << "1) Sa incerc altceva." << endl << "2) Sa ies din program." << endl;

    //Alegem o optiune
    int confirmare2;
    cin >> confirmare2;
    system("CLS");
    if(confirmare2 == 2) return false;


}

bool frecventa()
{
    //Prezentam optiunea aleasa.
    cout << "Ati ales sa cautati frecventa unui cuvant." << endl;

    //Intrebam userul de unde sa luam input-ul.

    cout << "Introduceti textul de la tastatura. (Maxim 500 caractere)" << endl << endl;


    //Cazul in care userul doreste sa introduca textul de la tastatura.

    //Creem un vector in care o sa stocam textul.
Scriere:
    char txt[501];              //Initializam vectorul.
    cout << "Introduceti textul: " << endl;

    cin.ignore();               //Ignoram acel new line pentru ca userul sa introduca textul dorit.
    cin.get(txt,501);           //Citim textul de la tastatura.
    cout << endl << endl;       //Padding
    system("CLS");              //Dam clear la consola.

    //Vrem o confirmare de la user ca textul este introdus corect.
Confirmare:
    cout << "Textul este acesta: " << endl;
    for(int i = 0 ; i < strlen(txt); i++) cout << txt[i];
    cout << endl << endl;

    //Verificam daca textul este introdus corect.
    cout << "Este textul scris corect? " << endl;
    cout << "1) Da." << endl << "2) Nu." << endl;

    //Alegem o optiune
    int confirmare;
    cin >> confirmare;
    system("CLS");

    //Daca textul este scris corect, transformam literele mici din text in litere mari.
    if(confirmare == 1)
    {
        //Intrebam userul ce cuvant sau litera doreste sa o caute.
        cout << "Ce cuvant sau litera doriti sa o cautati? " << endl;
        string cuv;
        cin >> cuv;                                                     //Aflam cuvantul.
        system("CLS");

        int nr = 0;                                                     //Initializam un contor.
        char* p = strtok(txt, " ");
        while(p != NULL)                                                //Sectionam textul in cuvinte.
        {

            if(p == cuv) nr++;                                          //Daca cuvantul sectionat din text este acelasi ca si cel cautat, contorul creste cu o unitate.
            p = strtok(NULL, " ");
        }
        cout << "Cuvantul pe care il cautati( " << cuv << " ) se afla in text de : " << nr << " ori." << endl;          //Afisam de cate ori apare cuvantul cautat.

    }
    else if(confirmare == 2)        //Daca textul nu a fost introdus corect, punem userul sa introduca din nou textul
    {
        goto Scriere;
    }
    else                            //Daca userul a introdus alta optiune, il punem sa aleaga o optiune buna.
    {
        goto Confirmare;
    }

    //Intrebam userul daca mai doreste sa incerce ceva sau sa iasa din program.
    cout << endl << endl << endl;
    cout << "Doriti sa incercati altceva sau sa iesiti din program? "  << endl;
    cout << "1) Sa incerc altceva." << endl << "2) Sa ies din program." << endl;

    //Alegem o optiune
    int confirmare2;
    cin >> confirmare2;
    system("CLS");
    if(confirmare2 == 2) return false;

}

bool nrConVoc()
{
    //Prezentam optiunea aleasa.
    cout << "Ati ales sa aflati numarul de vocale si de consoane din text." << endl;

    //Creem un vector in care o sa stocam textul.
Scriere:
    char txt[501];              //Initializam vectorul.
    cout << "Introduceti textul: " << endl;

    cin.ignore();               //Ignoram acel new line pentru ca userul sa introduca textul dorit.
    cin.get(txt,501);           //Citim textul de la tastatura.
    cout << endl << endl;       //Padding
    system("CLS");              //Dam clear la consola.

    //Vrem o confirmare de la user ca textul este introdus corect.
Confirmare:
    cout << "Textul este acesta: " << endl;
    for(int i = 0 ; i < strlen(txt); i++) cout << txt[i];
    cout << endl << endl;

    //Verificam daca textul este introdus corect.
    cout << "Este textul scris corect? " << endl;
    cout << "1) Da." << endl << "2) Nu." << endl;

    //Alegem o optiune
    int confirmare;
    cin >> confirmare;
    system("CLS");

    //Daca textul este scris corect, numaram consoanele si vocalele.
    if(confirmare == 1)
    {
        int nrC = 0;                                                    //Numar consoane
        int nrV = 0;                                                    //Numar vocale
        for(int i = 0; i < strlen(txt); i++)                            //Trecem prin fiecare litera a cuvantului
        {

            if(strchr("aeiou", txt[i])) nrV++;                          //Daca e vocala, contorul vocalelor creste cu o unitate
            else if(txt[i] == ' ') continue;                            //Daca e un spatiu, trecem peste el
            else nrC++;                                                 //Daca e consoana, contorul consoanelor creste cu o unitate

        }
        system("CLS");                                                  //Dam clear la consola.
        cout << "Numar vocale: " << nrV << " /  Numar Consoane: " << nrC << endl;   //Afisam numarul de vocale si de consoane.
    }

    //Intrebam userul daca mai doreste sa incerce ceva sau sa iasa din program.
    cout << endl << endl << endl;
    cout << "Doriti sa incercati altceva sau sa iesiti din program? "  << endl;
    cout << "1) Sa incerc altceva." << endl << "2) Sa ies din program." << endl;

    //Alegem o optiune
    int confirmare2;
    cin >> confirmare2;
    system("CLS");
    if(confirmare2 == 2) return false;

}

bool acronim()
{
    //Prezentam optiunea aleasa.
    cout << "Ati ales crearea unui acronim. "  << endl;

    //Creem un vector in care o sa stocam textul.
Scriere:
    char txt[501];              //Initializam vectorul.
    cout << "Introduceti textul: " << endl;

    cin.ignore();               //Ignoram acel new line pentru ca userul sa introduca textul dorit.
    cin.get(txt,501);           //Citim textul de la tastatura.
    cout << endl << endl;       //Padding
    system("CLS");              //Dam clear la consola.

    //Vrem o confirmare de la user ca textul este introdus corect.
Confirmare:
    cout << "Textul este acesta: " << endl;
    for(int i = 0 ; i < strlen(txt); i++) cout << txt[i];
    cout << endl << endl;

    //Verificam daca textul este introdus corect.
    cout << "Este textul scris corect? " << endl;
    cout << "1) Da." << endl << "2) Nu." << endl;

    //Alegem o optiune
    int confirmare;
    cin >> confirmare;
    system("CLS");

    //Daca textul este scris corect, transformam literele mici din text in litere mari.
    if(confirmare == 1)
    {
        char acr[20];
        int k=0;
        char* p = strtok(txt, " ");
        while(p != NULL)                    //Sectionam textul in cuvinte si stocam prima litera din fiecare cuvant in vectorul acr.
        {
            acr[k] = p[0];
            k++;
            p = strtok(NULL, " ");
        }

        for(int i = 0; i < k; i++)                                  //Trecem prin fiecare litera din vectorul acr si o afisam ca majuscula.
            if(i != k-1) cout << (char)toupper(acr[i]) << '.';
            else cout << (char)toupper(acr[i]);                     //Daca suntem la ultima litera nu mai afisam '.'

    }
    else if(confirmare == 2)        //Daca textul nu a fost introdus corect, punem userul sa introduca din nou textul.
    {
        goto Scriere;
    }
    else                            //Daca userul a introdus alta optiune, il punem sa aleaga o optiune buna.
    {
        goto Confirmare;
    }

    //Intrebam userul daca mai doreste sa incerce ceva sau sa iasa din program.
    cout << endl << endl << endl;
    cout << "Doriti sa incercati altceva sau sa iesiti din program? "  << endl;
    cout << "1) Sa incerc altceva." << endl << "2) Sa ies din program." << endl;

    //Alegem o optiune
    int confirmare2;
    cin >> confirmare2;
    system("CLS");
    if(confirmare2 == 2) return false;


}

int anagrame()
{
    //Prezentam optiunea aleasa.
    cout << "Ati ales verificarea daca 2 cuvinte sunt anagrame. "  << endl;

    //Creem un vector in care o sa stocam textul.
Scriere:
    cout << "Care sunt cuvintele?" << endl;
    string cuv1;
    cin >> cuv1;
    string cuv2;
    cin >> cuv2;

    cout << endl << endl;       //Padding
    system("CLS");              //Dam clear la consola.

    //Vrem o confirmare de la user ca textul este introdus corect.
Confirmare:
    cout << "Cuvintele sunt: " << endl << endl;
    cout << "Cuvantul 1: " << cuv1 << endl;
    cout << "Cuvantul 2: " << cuv2 << endl;


    cout << endl << endl;

    //Verificam daca textul este introdus corect.
    cout << "Sunt cuvintele scrise corect? " << endl;
    cout << "1) Da." << endl << "2) Nu." << endl;

    //Alegem o optiune
    int confirmare;
    cin >> confirmare;
    system("CLS");

    //Daca textul este scris corect, transformam literele mici din text in litere mari.
    if(confirmare == 1)
    {

        if(cuv1.length() != cuv2.length())                            //Daca cele 2 cuvinte nu au acelasi numar de litere, e clar ca nu sunt anagrame deci putem inchide programul dupa ce ii aratam ca nu sunt anagrame.
        {
            cout << "Cele 2 cuvinte nu sunt anagrame." ;
            getch();
            return 0;
        }


        bool anagrama = true;                                           //Presupunem ca cele 2 cuvinte sunt anagrame.

        sort(cuv1.begin(), cuv1.end());                                 //Sortam cei doi vectori de caractere(cuv1, cuv2)
        sort(cuv2.begin(), cuv2.end());

        for(int i = 0 ; i < cuv1.length(); i++)                         //Daca cele 2 litere nu corespund inseamna ca cele 2 cuvinte nu sunt anagrame.
            if(cuv1[i] != cuv2[i])
        {
            anagrama = false;
        }


        if(anagrama == true) cout << "Cele 2 cuvinte sunt anagrame." << endl;   //Daca cuvintele sunt anagrame, precizam acest lucru userului si invers.
            else cout << "Cele 2 cuvinte nu sunt anagrame" << endl;


    }
    else if(confirmare == 2)        //Daca textul nu a fost introdus corect, punem userul sa introduca din nou textul
    {
        goto Scriere;
    }
    else                            //Daca userul a introdus alta optiune, il punem sa aleaga o optiune buna.
    {
        goto Confirmare;
    }
    //Intrebam userul daca mai doreste sa incerce ceva sau sa iasa din program.
    cout << endl << endl << endl;
    cout << "Doriti sa incercati altceva sau sa iesiti din program? "  << endl;
    cout << "1) Sa incerc altceva." << endl << "2) Sa ies din program." << endl;

    //Alegem o optiune
    int confirmare2;
    cin >> confirmare2;
    system("CLS");
    if(confirmare2 == 2) return false;
}

int main()
{
    bool on = true;
    while( on == true)
    {
        int alegere = menu();
        if(alegere == 1) on = litMareInLitMica();
        if(alegere == 2) on = frecventa();
        if(alegere == 3) on = nrConVoc();
        if(alegere == 4) on = acronim();
        if(alegere == 5) on = anagrame();

    }
    return 0;
}
