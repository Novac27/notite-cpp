#include <iostream>

using namespace std;

int main()
{
    // 1) " | " este bitwise OR
    cout << " EXEMPLU pentru bitwise OR: " << endl;
    int a = 5;      //0101b in Base2 / (0x05) in Base16
    int b = 12;     //1100b in Base2 / (0x0C) in Base16
    int c = a | b;  //1101b in Base2 / (0x0D) in Base16

    cout << "a = " << a << " , b = " << b << " , c = " << c << endl;

    //Un bitwise OR face operatii la nivel de biti si foloseste aceleasi reguli ca la ||(OR).
    /*
        true OR true = true
        true OR false = true
        false OR false = false
    */

    /* Cand trebuie sa lucram cu biti, o sa transformam numarul din Base10 in Base2 si vedem la nivel de biti, unul sub altul, ce se intampla.

        int a = 0 1 0 1
        int b = 1 1 0 0 |
                ---------
        int c = 1 1 0 1

        (0 OR 1 = 1 ; 1 OR 1 = 1 ; 0 OR 0 = 0 ; 1 OR 0 = 1)
    */

    // 2) " ^ " este bitwise XOR(exclusive OR)
    cout << " EXEMPLU pentru bitwise XOR: " << endl;
    int d = 5;      //0101b in Base2 / (0x05) in Base16
    int e = 9;      //1001b in Base2 / (0x09) in Base16
    int f = d ^ e;  //1100b in Base2 / (0x0C) in Base16

    cout << "d = " << d << " , e = " << e << " , f = " << f << endl;

    //Un bitwise XOR face operatii la nivel de biti si foloseste urmatoarele reguli de adevar:
    /*  true OR true = false
        true OR false = true
        false OR false = false
    */

    /* Cand trebuie sa lucram cu biti, o sa transformam numarul din Base10 in Base2 si vedem la nivel de biti, unul sub altul, ce se intampla.

        int a = 0 1 0 1
        int b = 1 0 0 1 ^
                ---------
        int c = 1 1 0 0

    */

    // Bitwise XOR este foarte folosit in operatiile cu biti pentru encriptie si compresie.

    // Exemplu XOR: Schimbarea a 2 valori fara o variabila temporara\

    cout << " EXEMPLU pentru XOR swap:" << endl;
    int g = 42;
    int h = 64;

    // XOR swap
    g = g ^ h;
    h = h ^ g;
    g = g ^ h;

    cout << "g = " << g << " , h = " << h <<  endl;


    // 3) " & " bitwise AND
    cout << " EXEMPLU pentru bitwise AND:" << endl;
    int i = 6;      // 0110b in Base2 / (0x06) in Base16
    int j = 10;     // 1010b in Base2 / (0x0A) in Base16
    int k = i & b;  // 0010b in Base2 / (0x02) in Base2

    cout << "i = " << i << " , j = " << j << " , k = " << k << endl;

    //Un bitwise AND lucreaza la nivel de biti si foloseste aceleasi reguli de adevar ca la &&(AND):
    /*  true AND true = true
        true AND false = False
        false AND false = false
    */

    /* Cand trebuie sa lucram cu biti, o sa transformam numarul din Base10 in Base2 si vedem la nivel de biti, unul sub altul, ce se intampla.

        int a = 0 1 1 0
        int b = 1 0 1 0 &
                ---------
        int c = 0 0 1 0
    */

    // 4) " << " este un LEFT SHIFT
    cout << " EXEMPLU pentru LEFT SHIFT: " << endl;
    int l = 1;          //0001b in Base2
    int m = a << 1;     //0010b in Base2

    cout << "l = " << l << " , m = " << m << endl;

    // Un LEFT SHIFT o sa mute numarul L la stanga cu 1 bit in cazul nostru. La mutare o sa se adauge 0 in dreapta. Exemplu: 5 (0000 0101) << 4 = 80 (0101 0000)
    // Un LEFT SHIFT este echivalent cu inmultirea cu 2.
    // Un LEFT SHIFT cu numere negative este nedefinit.

    //5) " >> " este un RIGHT SHIFT
    cout << " EXEMPLU pentru RIGHT SHIFT: " << endl;
    int o = 2;          //0010b in Base2
    int p = o >> 1;     //0001b in Base2

    cout << "o = " << o << " , p = " << p << endl;

    // Un RIGHT SHIFT o sa mute numarul O cu 1 bit in cazul nostru.




    return 0;
}
