 **1. Algoritmi**

**Ce este un algoritm?**

    Algoritmul este metoda de solutionare a unui tip de probleme, constand intr-o multime finita, bine definita si ordonata de operatii. 

**Proprietatile algoritmului**

    1) Generalitate –algoritmul rezolva o clasa de probleme nu o problema particulara
    Ex : Nu 3+2 ci a+b
    2) Claritate – algoritmul nu contine ambiguitati
    3) Finitudine – algoritmul se termina dupa un numar finit de pasi
    4) Completitudinea – algoritmul tine cont de toate cazurile particulare ale problemei
    generale. Ex : calculul lui 2 la n. Caz particular : 2 la 0 care trebuie tratat separat.
    5) Eficienta – algoritmul se va executa cu numar minim de pasi, folosind un minim
    de memorie
    6) Realizabilitatea – sa poata fi codificat intr-un limbaj de programare

**2. Obiecte cu care lucreaza algoritmii**

2.1)  **Date (variabile sau constante)**

Pot fi date de doua tipuri:

    • **Elementare**(intregi, reale, logice, caracter)
    
    • **Structurate**

De asemenea datele pot fi:

• **Constante** = informatii care se autodefinesc; date care nu isi modifica valoarea;


    1.  Constante numerice: intregi: 2, 4; 
    2.  Reale: 5.2, 7.8
    3.  Constante logice: TRUE, FALSE
    4.  Constante Caracter: ‘c’, ‘\n’
    5.  Constante sir de caractere : “mama are mere”, “Cuvantul \”while\” reprezinta cuvant cheie pentru
    limbajul c “

• **Variabile** = date care isi modifica valoarea. Pot fi de aceleasi tipuri ca si constantele.

**Proprietatile variabilelor:** nume, tip, locatie de memorie, valoare la un moment dat.

2.2) **Operatori**

    Operatorii au rolul de a preciza ce operatii se vor realiza asupra datelor cu care lucram.
    Operatorii se aplica doar anumitor tipuri de date. De pilda nu putem aduna doua caractere sau nu
    putem imparti cu rest numerele reale (cele cu virgula). 

**Tipuri de operatori:**

**2.2.1) Matematici :**

    Operatorii matematici sunt: + , - , * , /
    In plus vom avea doi operatori: 
        div = catul impartirii a doua numere intregi
        mod = restul impartirii a doua numere intregi
**2.2.2) Relationali:**       
    < , > , >= , <= , = , <> (diferit)
    
Aceste expresii au ca rezultat o valoare logica (adevarat sau fals). De aceea vom spune despre
acestea ca sunt expresii logice chiar daca nu contin operatori logici.

**2.2.3) Logici: AND/OR/NOT**

| **a** | **b** | **a AND b**| **a OR b** | **NOT a** |
| ------ | ------ | ------ | ------ | ------ |
| TRUE | TRUE | TRUE | TRUE | FALSE | cell | 
| TRUE | FALSE | FALSE | TRUE |  FALSE | cell | 
| FALSE | TRUE | FALSE | TRUE | TRUE | cell | 
| FALSE | FALSE | FALSE | FALSE |  TRUE | cell | 

**Proprietati (Relatiile lui Morgan):**

    1) NOT (a AND b)=NOTa OR NOTb
    2) NOT (a OR b)=NOTa AND NOTb
    
**2.3) Expresii**

Ca si la matematica, o expresie este formata din operanzi si operatori. In functie de tipul de
date, vor fi aplicati operatori specifici. De pilda, are sens sa aplicam un operator aritmetic asupra
unor date numerice. Nu are sens o operatie de adunare asupra a doua date caracter. Cum nu
are sens de asemenea sa aplicam un operator logic asupra a 2 numere: ce sens ar avea
expresia : “2 si 3” ?
Expresiile se pot clasifica si ele in functie de valoarea rezultata in urma evaluarii expresiei. 

**Vom avea :**

    1. expresii intregi. ex: 2+3; 4*50; 30+a, unde a este o variabila numerica de tip intreg
    2. expresii reale. ex: 2.3+4, 2+x, unde x este o variabila numerica de tip real
    3. expresii logice. Ex: a>b AND 4<c, c%2=1

**Evaluarea unei expresii**

Pentru evaluarea expresiilor se respecta regulile de baza invatate la matematica. Se evalueaza
intai expresiile dintre parantezele rotunde, apoi se executa operatiile in ordinea prioritatii lor.
Daca exista operatii cu aceasi prioritate, ele se executa in ordine, in functie de asociativitatea lor.
Prioritatea 1 este considerata cea mai mare.

**Prioritate** | **Operatori** | **Simbol** | **Asociativitate**
-----------|-----------|--------|----------------
1 | Negatia logica | NOT | De la stanga la dreapta
2 | Aritmetici multiplicativi | *, /, MOD, DIV | De la stanga la dreapta
3 | Aritmetici aditivi | +, - | De la stanga la dreapta
4 | Relationali | <, >, <=, >=, !=, == | De la stanga la dreapta
5 | Conjunctia logica (si) | AND | De la stanga la dreapta
6 | Disjunctia logica (sau) | OR | De la stanga la dreapta

**3. Reprezentarea algoritmilor**

 Algoritmul “oua ochiuri” este un algoritm pe care il va executa un om. Instructiunea “Pune tigaia pe foc” nu ar putea fi executata de catre calculator. De aceea algoritmii pe care ii vom studia la informatica vor contine doar operatii pe care un calculator le-ar putea executa. 

** Aceste operatii pot fi:**

    • Operatii de intrare/iesire =operatiile de citire/scriere 
    • Operatii de atribuire 
    • Operatii decizionale 
    
**3.1)  Pseudocodul**

Pseudocodul este un limbaj apropiat limbajului nostru natural, dar care este de asemenea foarte apropiat si de limbajele de programare in care vor fi transpusi algoritmii.

|**Descriere**| **Pseudocod**|
|-------------|--------------|
|Blocul de citire - se citesc de la dispozitivul de intrare valorile variabilelor specificate in lista variabile ( separate prin virgula )| Citeste lista_variabile <br> Citeste x  <br> Citeste x, y |
|Blocul de scriere ( doua variante ) - se scriu la dispozitivul de iesire valorile obtinute in urma evaluarii expresiilor din lista( separate prin virgula ) | Scrie lista_expresii <br> Scrie y |
|Blocul de atribuire - se evalueaza expresia; valoarea obtinuta este memorata in variabila, vechea valoare pierzandu-se | Variabila <-- expresie <br> x  <-- x+1|
|Blocul de decizie - se evalueaza conditia: daca e adevarata se continua cu prelucrarea indicata de ramura da, altfel cu ramura nu; conditia poate contine operatori relationali, operatori logici | daca (conditie) ... <br> daca (x ≥ y + 3 )... |







http://lbi.ro/~bella/5/cap%2004%20Algoritmi%20-%20generalitati/02_algoritmi_S4_S5.pdf





C++ este un limbaj de programare general, compilat. Este un limbaj multi-paradigmă, cu verificarea statică a tipului variabilelor ce suportă programare procedurală, abstractizare a datelor, programare orientată pe obiecte. În anii 1990, C++ a devenit unul dintre cele mai populare limbaje de programare comerciale, rămânând astfel până azi.

Bjarne Stroustrup de la Bell Labs a dezvoltat C++ (inițial denumit C cu clase) în anii 1980, ca o serie de îmbunătățiri ale limbajului C. Acestea au început cu adăugarea noțiunii de clase, apoi de funcții virtuale, suprascrierea operatorilor, moștenire multiplă, șabloane (engleză template) și excepții. Limbajul de programare C++ a fost standardizat în 1998 ca și ISO 14882:1998, versiunea curentă fiind din 2003, ISO 14882:2003. Următoarea versiune standard, cunoscută informal ca C++0x, este în lucru. 

	Exemple:

Program care afișează textul "Hello World":	

	#include <iostream>

	using namespace std;

	int main() {
	    cout << "Hello World" << endl;
	    return 0;
	}



Algoritm pentru aflarea celui mai mare divizor comun (algoritmul lui Euclid): 

	#include<iostream>

	using namespace std;

	int main() {
	    int a, b, r;

	    cin >> a;
	    cin >> b;

	    r = a % b;
	    while(r) {
	        a = b;
	        b = r;
	        r = a % b;
	    }

	    cout << b << endl;
	    return 0;
	}


