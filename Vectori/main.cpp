#include <iostream>
#include <vector>
using namespace std;

int main()
{
    vector<int> v{ 1, 2, 3 };

    //folosim [] ca sa accesam elementele.
    int a = v[1]; // a = 2
    v[1] = 4;     // v contine acum { 1, 4, 3 }

    vector<int> x{ 4, 5, 6};

    int b = x.front();  // b este 4. x.front() este echivalentul lui x[0]
    x.front() = 3;      // x contine acum { 3, 4, 3}
    int c = x.back();   //c este 6.  x.back() este echivalentul lui x[x.size()-1]
    x.back() = 7;       // x acum contine { 3, 5, 7}



    vector<int> z;
    int sum = 0;

    for(int i = 1; i <= 10; i++) z.push_back(i); //creeaza si initializeaza vectorul.

    while(!z.empty())     //da loop pana nu mai am elemente.
    {
        sum += z.back();  //adauga ultimul element la suma.
        z.pop_back();     //elimina ultimul element din vector.
    }

    cout << "total: " << sum << endl;  //aratam suma.




    return 0;
}
